<?php

namespace Achse\Languages\Elements;


use Nette\InvalidArgumentException;
use Nette\Object;

/**
 * Class Word
 * @package App\Business\Languages\Elements
 * @author Hejna Petr <hejna.peter@gmail.com>
 */
class Word extends Object {

    protected $count = 1;

    /** @var Word */
    protected $more = NULL;

    /** @var Word */
    protected $less = NULL;

    protected $text = "";

    protected $prefix;


    protected $suffix;

    public function __construct($text, $count = NULL) {
        $this->text = $text;
        $this->count = $count;
    }

    function __clone() {
        if ($this->less !== NULL) {
            $this->less = clone $this->less;
        }

        if ($this->more !== NULL) {
            $this->more = clone $this->more;
        }
    }

    /****** API ****/

    public function isLeaf() {
        return $this->count === NULL AND $this->more === NULL AND $this->less === NULL;
    }


    /****** GETTERS AND SETTERS ******/

    /**
     * @param $count
     * @throws \Nette\InvalidArgumentException
     */
    public function setCount($count) {
        if (!is_int($count)) {
            throw new InvalidArgumentException("\$count has to be integer.");
        }
        $this->count = $count;
    }

    /**
     * @return int|null
     */
    public function getCount() {
        return $this->count;
    }


    /**
     * @param Word $less
     */
    public function setLess(Word $less) {
        $this->less = $less;
    }

    /**
     * @return Word|null
     */
    public function getLess() {
        return $this->less;
    }


    /**
     * @param Word $more
     */
    public function setMore(Word $more) {
        $this->more = $more;
    }

    /**
     * @return Word|null
     */
    public function getMore() {
        return $this->more;
    }

    /**
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->prefix . $this->text . $this->suffix;
    }


    /**
     * @param string $prefix
     * @param bool $recursive
     */
    public function setPrefix($prefix, $recursive = true) {
        if ($recursive) {
            if ($this->more) {
                $this->more->setPrefix($prefix, $recursive);
            }
            if ($this->less) {
                $this->less->setPrefix($prefix, $recursive);
            }
        }
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getPrefix() {
        return $this->prefix;
    }


    /**
     * @param $suffix
     * @param bool $recursive
     */
    public function setSuffix($suffix, $recursive = true) {
        if ($recursive) {
            if ($this->more) {
                $this->more->setSuffix($suffix, $recursive);
            }
            if ($this->less) {
                $this->less->setSuffix($suffix, $recursive);
            }
        }
        $this->suffix = $suffix;
    }

    /**
     * @return mixed
     */
    public function getSuffix() {
        return $this->suffix;
    }

}