<?php

namespace Achse\Languages;

/**
 * Class Language
 * @package Achse\Languages
 *
 */
abstract class Language implements ILanguage {

    protected $decimalSeparator;

    protected $thousandsSeparator;

    /**
     * @param string $thousandsSeparator
     */
    public function setThousandsSeparator($thousandsSeparator) {
        $this->thousandsSeparator = $thousandsSeparator;
    }

    /**
     * @return string
     */
    public function getThousandsSeparator() {
        return $this->thousandsSeparator;
    }

    /**
     * @param string $decimalSeparator
     */
    public function setDecimalSeparator($decimalSeparator) {
        $this->decimalSeparator = $decimalSeparator;
    }

    /**
     * @return string
     */
    public function getDecimalSeparator() {
        return $this->decimalSeparator;
    }

} 