<?php

namespace Achse\Languages;

use Nette\DateTime;

/**
 * Class ILanguage
 * @package App\Business\Languages
 * @author Petr Hejna
 */
interface ILanguage {

    /**
     * @param $word
     * @param null $count
     * @return mixed
     */
    public function translate($word, $count = NULL);

    /**
     * @param float $number
     * @return double
     */
    public function formatFloatNumber($number);

    /**
     * @param DateTime $datetime
     * @param bool $time
     * @param bool $seconds
     * @return string
     */
    public function formatDate(DateTime $datetime, $time = true, $seconds = false);

    /**
     * @param string $thousandsSeparator
     */
    public function setThousandsSeparator($thousandsSeparator);

    /**
     * @return string
     */
    public function getThousandsSeparator();

    /**
     * @param string $decimalSeparator
     */
    public function setDecimalSeparator($decimalSeparator);

    /**
     * @return string
     */
    public function getDecimalSeparator();

}