<?php

namespace Achse\Languages;

use Nette\DateTime;
use Nette\InvalidArgumentException;

/**
 * @author Petr Hejna <hejna.peter@gmail.com>
 */
class Translator implements \Nette\Localization\ITranslator {

    /**
     * @var string Actual - seleced language
     */
    protected $lang;

    /**
     * @var array An array of possible languages.
     */
    protected $langs = array(
        'cs_CZ',
        // 'en_US',
    );

    /**
     * @var array Array of translators, each language have an instance in this repository..
     */
    protected $languagesTranslators = array();

    public function __construct() {
        \Achse\Utils\Time::$translator = $this;
    }

    /**
     * This is factory for language translators. This method creates lazy new instances and keep
     * them in poll.
     *
     * @return ILanguage
     */
    protected function getTranslator() {
        if (!isset ($this->translations[$this->lang])) {

            if ($this->lang == 'cs_CZ') {
                $this->translations[$this->lang] = new CzechLanguage();
            }
        }

        return $this->translations[$this->lang];
    }

    /**
     * Translates the given string.
     * @param $message
     * @param  int plural count
     * @throws LanguageNotSetException
     * @return string
     */
    public function translate($message, $count = NULL) {
        if (!$this->lang) {
            throw new LanguageNotSetException("Call setLang() before translate method.");
        }

        /**@var $tr ILanguage */
        $tr = $this->getTranslator();

        return $tr->translate($message, $count);
    }

    /**
     * Sets a language for application, only an language from $this->langs can be used.
     *
     * @param $lang
     * @throws \Nette\InvalidArgumentException
     */
    public function setLang($lang) {
        if (!in_array($lang, $this->langs)) {
            throw new InvalidArgumentException("Unsupported localization '{$lang}'.");
        }
        $this->lang = $lang;
    }

    /**
     * @return string A current language string identificator from $this->langs namespace.
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Formats date.
     *
     * @param $datetime
     * @param bool $time If true (default) the time is visible. False means that only date will be displayed.
     * @param bool $seconds False (default) means no seconds will be displayed. True shos seconds.
     * @return string
     */
    public function formatDate($datetime, $time = true, $seconds = false) {
        if (!$datetime) {
            return;
        }
        
        if (is_numeric($datetime)) {
            $datetime = new DateTime($datetime);
        }
        
        return $this->getTranslator()->formatDate($datetime, $time, $seconds);
    }

    /**
     * Format a float numbers.
     *
     * @param $number
     * @return float
     */
    public function formatFloatNumber($number) {
        return $this->getTranslator()->formatFloatNumber($number);
    }

    /**
     * Formats a money number.
     *
     * @param $number
     * @param int $decimals
     * @return mixed
     */
    public function formatMoneyNumber($number, $decimals = 2) {
        return $this->getTranslator()->formatMoneyNumber($number, $decimals);
    }

}