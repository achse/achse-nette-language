<?php
namespace Achse\Languages;

interface ITranslationBaseElementControl {

    /**
     * @return Translator
     */
    public function getTranslator();

    /**
     * @param $translator Translator
     */
    public function setTranslator($translator);

} 