<?php

namespace Achse\Languages;

/**
 * Class LanguageNotSetException
 * Thrown when called uninitialized Translator
 *
 * @author Petr Hejna
 */

class LanguageNotSetException extends \Exception {

}
