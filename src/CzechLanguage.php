<?php

namespace Achse\Languages;

use Achse\Languages\Elements\Word;
use Achse\Utils\Time;
use Nette\DateTime;

/**
 * Class CzechLanguage
 * @package App\Business\Languages
 * @author Hejna Petr <hejna.peter@gmail.com>
 */
class CzechLanguage extends Language {

    protected $data;

    public function __construct() {
        $this->setThousandsSeparator(" ");
        $this->setDecimalSeparator(",");

        // Here should be a words loading
        // Set translatations for Time
        Time::$STRINGS['in'] = 'za';
        Time::$STRINGS['ago'] = 'před';

        Time::$STRINGS['second_in'] = "sekunda";
        Time::$STRINGS['minute_in'] = "minuta";
        Time::$STRINGS['hour_in'] = "hodina";
        Time::$STRINGS['day_in'] = "den";
        Time::$STRINGS['week_in'] = "týden";
        Time::$STRINGS['month_in'] = "měsíc";
        Time::$STRINGS['year_in'] = "rok";

        Time::$STRINGS['second_ago'] = "sekunda_7";
        Time::$STRINGS['minute_ago'] = "minuta_7";
        Time::$STRINGS['hour_ago'] = "hodina_7";
        Time::$STRINGS['day_ago'] = "den_7";
        Time::$STRINGS['week_ago'] = "týden_7";
        Time::$STRINGS['month_ago'] = "měsíc_7";
        Time::$STRINGS['year_ago'] = "rok_7";

        Time::$STRINGS['inText'] = "%in% %d %NAME%";
        Time::$STRINGS['agoText'] = "%ago% %d %NAME%";

        $this->generateClassic_1_234_5('sekunda', 'sekunda', 'sekundy', 'sekund');
        $this->generateClassic_1_234_5('minuta', 'minuta', 'minuty', 'minut');
        $this->generateClassic_1_234_5('hodina', 'hodina', 'hodiny', 'hodin');
        $this->generateClassic_1_234_5('den', 'den', 'dny', 'dnů');
        $this->generateClassic_1_234_5('týden', 'týden', 'týdny', 'týdnů');
        $this->generateClassic_1_234_5('měsíc', 'měsíc', 'měsice', 'měsíců');
        $this->generateClassic_1_234_5('rok', 'rok', 'roky', 'let');

        $this->generateClassic_1_25('sekunda_7', 'sekundou', 'sekundami');
        $this->generateClassic_1_25('minuta_7', 'minutou', 'minutami');
        $this->generateClassic_1_25('hodina_7', 'hodinou', 'hodinami');
        $this->generateClassic_1_25('den_7', 'dnem', 'dny');
        $this->generateClassic_1_25('týden_7', 'týdnem', 'týdny');
        $this->generateClassic_1_25('měsíc_7', 'měsícem', 'měsíci');
        $this->generateClassic_1_25('rok_7', 'rokem', 'lety');
    }

    /**
     * Toto je případ, kdy jedna se liší od 2-4, a 5 a více se liší od obou
     *
     * @param $base
     * @param $one
     * @param $twoToFour
     * @param $five
     */
    protected function generateClassic_1_234_5($base, $one, $twoToFour, $five) {
        $this->data[$base] = $_3 = new Word($twoToFour, 3);
        $_1 = new Word($one, 1);
        $_4 = new Word($twoToFour, 4);

        $_3->setLess($_1);
        $_3->setMore($_4);

        $_4->setMore(new Word($five));

        $_1->setMore(new Word($twoToFour));
        $_1->setLess(new Word($five)); // same as five // 0 lidí, -1 lidí, .... -3 lidé, but fuck this shit :D
    }

    /**
     * Tohle generuje když tvar pro 1-4 je stejný a liší se až pět a více.
     * 0 je stejná jako 5 a více
     *
     * @param $base
     * @param $oneToFour
     * @param $five
     */
    protected function generateClassic_14_5($base, $oneToFour, $five) {
        $this->data[$base] = $_1 = new Word($oneToFour, 1);
        $_5 = new Word($five, 5);
        $_0 = new Word($five, 0);

        $_1->setMore($_5);
        $_1->setLess(new Word($five));

        $_5->setMore(new Word($five));
        $_5->setLess(new Word($oneToFour));
    }

    /**
     * Tento případ nastává kdy pouze jednička je jiná
     *
     * @param $base
     * @param $one
     * @param $twoToFive
     */
    protected function generateClassic_1_25($base, $one, $twoToFive) {
        $this->data[$base] = $_1 = new Word($one, 1);
        $_1->setMore(new Word($twoToFive));
        $_1->setLess(new Word($twoToFive));
    }

    public function translate($word, $count = NULL) {
        if ($count === NULL)  {
            return $word;

        } elseif (isset ($this->data[$word])) {
            /** @var $curr Word */
            $curr = $this->data[$word];

            while (true) {

                if ($count == $curr->getCount()) {
                    return $curr->getText();

                } elseif ($curr->isLeaf()) {
                    return $curr->getText();

                } else {
                    $curr = ( $count > $curr->getCount() ? $curr->getMore() : $curr->getLess() );
                }
            }

        } else {
            return $word;
        }
    }

    public function formatFloatNumber($number) {
        return str_replace('.', $this->decimalSeparator, $number);
    }

    public function formatMoneyNumber($number, $decimals = 2) {

        if (!is_numeric($number)) {
            return $number;
        }

        $number = number_format($number, $decimals, '.' , $this->thousandsSeparator);

        return $this->formatFloatNumber($number);
    }

    public function formatDate(DateTime $datetime, $time = true, $seconds = false) {
        // TODO: reimplement this ugliness
        $month = (int) $datetime->format('m');
        return strftime("%e. {$month}. %Y " . ($time ? ("%H:%M" . ($seconds ? ":%S" : "")) : ""), $datetime->getTimestamp());
    }

}